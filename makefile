
# Include paths
IPATH= -I/usr/local/include/libxml++-2.6 -I/usr/local/lib/libxml++-2.6/include \
  -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include \
  -I/usr/include/glibmm-2.4 -I/usr/lib/glibmm-2.4/include \
  -I./include


# Library paths
LDPATH= -L./LIB -L/usr/local/lib -L/usr/local/include \
  -L/usr/include/c++/4.5.1 -L/usr/include/c++/4.8.2


# Libraries
LDLIBS= -lxml++-2.6 -lglibmm-2.4 -lfcgi


# Compiler C flags
CFLAGS= $(IPATH) -Werror-implicit-function-declaration -funsigned-char \
  -D__LINUX__ -DNDEBUG -ggdb -std=gnu99 -pipe -m32


# Compiler C++ flags
CPPFLAGS= $(IPATH) -O0 -pipe -fmessage-length=0 -MMD -MP -funsigned-char \
  -D__LINUX__ -DNDEBUG -ggdb -Werror -std=c++11 -m32


# Linker flags
LDFLAGS= $(LDPATH) -m32


# Object list (C)
COBJS=  main.o


# Object list (C++)
CPPOBJS=


# Object list (others)
OTHOBJS=


# Build binary
all: test.fcgi
test.fcgi: $(COBJS) $(CPPOBJS)
	g++ $(COBJS) $(CPPOBJS) -o $@ $(LDFLAGS) $(LDLIBS) $(OTHOBJS)


# Cleanup
clean:
	@find . -name \*\.o -exec rm -f {} \;
	@find . -name \*[^.].d -exec rm -f {} \;


# Auto dependencies
DEPFLAGS= -MT $@ -MMD -MP -MF $*.Td
POSTCOMPILE=@mv -f $*.Td $*.d && touch $@
%.o : %.c %.d
	$(CC) $(DEPFLAGS) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<
	$(POSTCOMPILE)
%.o : %.cpp %.d
	$(CXX) $(DEPFLAGS) $(CPPFLAGS) -c -o $@ $<
	$(POSTCOMPILE) 
%.d: ;
DEPS=$(COBJS:.o=.d) $(CPPOBJS:.o=.d)
-include $(DEPS)
