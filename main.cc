#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcgi_stdio.h>

inline int safeatoi(
    const char *x)
{
  if (x)
    return std::atoi(x);
  return 0;
}

int main(void)
{
    int count = 0;
    while(FCGI_Accept() >= 0) {
      const char * method= getenv("REQUEST_METHOD");
      if (method && !strcmp(method, "POST")) {
        int ilen= safeatoi(getenv("CONTENT_LENGTH"));
        char *bufp= (char*) malloc(ilen);
        fread(bufp, ilen, 1, stdin);

        fflush(stdout);
//        printf("%s", "Status: HTTP 500 - Internal server error\r\n");
//        printf("%s", "Content-Type: text/plain\r\n\r\n");//Solved: “End of script output before headers”
//        printf("%s", "hello world");//application/xml  text/plain
        printf("%s","Status: HTTP 500 - Internal server error\r\nContent-Type: text/plain\r\n\r\nSome message body\nHello world fcgi");
        free(bufp);
      }
    }
    exit(0);
    return 0;
}
